`timescale 1ns/ 100 ps
module clk (clk);

output clk;
reg clk;

initial begin
clk = 0;
end

always begin
 #20 clk = !clk;
end

endmodule