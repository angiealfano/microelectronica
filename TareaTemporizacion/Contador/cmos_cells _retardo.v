module BUF(A, Y);
input A;
output Y;
assign  Y = A;

specify
	specparam tpdh = 1.5 ,tpdl=  1.5; //SN74LVC1G34 
	(A => Y) = (tpdh, tpdl);

endspecify

endmodule



module NOT(A, Y);
input A;
output Y;
assign Y = ~A;
specify
	specparam tpdh = 5, tpdl= 5; //SN74AHC1GU04
	(A *> Y) = (tpdh, tpdl);

endspecify
endmodule

module NAND(A, B, Y);
input A, B;
output Y;
assign Y = ~(A & B);

specify
	specparam tpdh = 1,tpdl= 1; //SN74LVC1G00 
	(A *> Y) = (tpdh, tpdl);
	(B *> Y) = (tpdh, tpdl);

endspecify
endmodule

module NOR(A, B, Y);   //SN74AHC1G02-EP
input A, B;
output Y;
assign Y = ~(A | B);

specify
	specparam tpdh = 8.1 ,tpdl= 8.1;
	(A *> Y) = (tpdh, tpdl);
	(B *> Y) = (tpdh, tpdl);

endspecify
endmodule


module NAND3(A, B, C, Y); SN74LVC1G10
input A, B, C;
output Y;
assign Y = ~(A & B & C);
specify
	specparam tpdh = 1.4,tpdl=  1.4;
	(A >* Y) = (tpdh, tpdl);
	(B *> Y) = (tpdh, tpdl);
	(C *> Y) = (tpdh, tpdl);

endspecify
endmodule

module NOR3 (A, B, C, Y); //SN74LVC1G27DSFR
input A, B, C;
output Y;
assign Y = ~(A | B | C);

specify
	specparam tpdh = 1.3 ,tpdl= 1.3;
	(A *> Y) = (tpdh, tpdl);
	(B *> Y) = (tpdh, tpdl);
	(C *> Y) = (tpdh, tpdl);

endspecify
endmodule


module DFF(C, D, Q);
input C, D;
output reg Q;
always @(posedge C)
	Q <= D;

    specify
      $setup(D, posedge C, 2);
      $hold(posedge C, D, 0.5);
   endspecify

endmodule

module DFFSR(C, D, Q, S, R);
input C, D, S, R;
output reg Q;
always @(posedge C, posedge S, posedge R) //BUscat esto
	if (S)
		Q <= 1'b1;
	else if (R)
		Q <= 1'b0;
	else
		Q <= D;

endmodule