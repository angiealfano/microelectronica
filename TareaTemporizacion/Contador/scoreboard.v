//Scoreboard
module scoreboard (    
    input enable,
    input [3:0] D,
    input [1:0] mode,

    input clk,
    input reset

    ) ;

reg [3:0] contador = 4'b0000;
reg RCO_aux;
reg load_aux;
wire [3:0] sb_Q_checker;
 reg sb_rco;
 reg sb_load;
 reg [3:0] sb_Q;
assign sb_Q_checker = sb_Q;
always @(posedge clk)
begin

    if (reset) begin

        sb_Q<=0;
        sb_rco<=0;
        sb_load<=0;
	contador<=0;
    end

    else if (!reset && !enable) begin
       sb_Q<=4'bz;
    end

    else begin
    sb_rco<=RCO_aux;
    sb_load<=load_aux;
    if(mode==2'b11) begin
        sb_Q<=D;
    end else begin
        sb_Q<=0;
        contador<=0;
    end
  
  if (enable) begin
    case (mode)
      2'b00: begin
        contador<=contador+3;
        sb_Q<=contador+3;
      end
      2'b01: begin
        contador<=contador-1;
        sb_Q<=contador-1;
      end
      2'b10: begin
        contador<=contador+1;
        sb_Q<=contador+1;
      end
      2'b11: begin
        sb_Q<=D;
        contador<=D;
        end
    endcase
  end
  else begin
    sb_Q<=0;
    contador<=0;
  end

end
end

always @(*) begin
    RCO_aux=0;
    load_aux=0;
    if(enable && mode==2'b00 && sb_Q>12) begin //Voy sumando de 3 en 3 
        RCO_aux=1;
    end else if(enable && mode==2'b01 && sb_Q==4'b0000) begin //Voy restando de 1 en 1 
        RCO_aux=1;
    end else if(enable && mode==2'b10 && sb_Q==4'b1111) begin //Voy sumando de 1 en 1 
        RCO_aux=1;
    end else if(enable && mode==2'b11) begin  //Estado de carga
        load_aux=1;
    end
end



endmodule

