module countersynth (    
    input enable,
    input [3:0] D,
    input [1:0] mode,

    input clk,
    input reset,
    output reg rco,
    output reg load,
    output reg [3:0] Q,

    ) ;

reg [3:0] contador = 4'b0000;
reg RCO_aux;
reg load_aux;
reg [3:0] highz = 4'bz;
wire [3:0] sb_Q_checker;
assign sb_Q_checker = Q;
always @(posedge clk)
begin

    if (reset) begin
        Q<=0;
        rco<=0;
        load<=0;
	      contador<=0;
    end

    else if (!reset && !enable) begin
       Q<=highz;
    end

    else begin
    rco<=RCO_aux;
    load<=load_aux;
    if(mode==2'b11) begin
        Q<=D;
    end else begin
        Q<=0;
        contador<=0;
    end
  
  if (enable) begin
    case (mode)
      2'b00: begin
        contador<=contador+3;
        Q<=contador+3;
      end
      2'b01: begin
        contador<=contador-1;
        Q<=contador-1;
      end
      2'b10: begin
        contador<=contador+1;
        Q<=contador+1;
      end
      2'b11: begin
        Q<=D;
        contador<=D;
        end
    endcase
  end
  else begin
    Q<=0;
    contador<=0;
  end

end
end

always @(*) begin
    RCO_aux=0;
    load_aux=0;
    if(enable && mode==2'b00 && Q>12) begin //Voy sumando de 3 en 3 
        RCO_aux=1;
    end else if(enable && mode==2'b01 && Q==4'b0000) begin //Voy restando de 1 en 1 
        RCO_aux=1;
    end else if(enable && mode==2'b10 && Q==4'b1111) begin //Voy sumando de 1 en 1 
        RCO_aux=1;
    end else if(enable && mode==2'b11) begin  //Estado de carga
        load_aux=1;
    end
end



endmodule

