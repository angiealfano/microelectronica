`include "clock.v"
`include "contador_synth.v"
`include "contador_tb5.v"
`include "cmos_cells.v"


module tb_top;

clk clocks (.clk (clk));

wire [3:0] Q;
wire [3:0] D;
wire [3:0] sb_Q;
wire [1:0] mode;
wire rco;
wire reset;
wire sb_load;

contador_tb tb(
    .clk (clk),
    .enable (enable),
    .D (D),
    .mode (mode),
    .Q (Q),
    .rco (rco),
    .reset(reset),
    .load (load)
);

countersynth dut (
    .enable (enable),
    .clk (clk),
    .reset (reset),
    .mode (mode),
    .D (D),
    .rco (rco),
    .load (load),
    .Q (Q)
);


endmodule
