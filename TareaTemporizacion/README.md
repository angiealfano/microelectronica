Si se desean correr las pruebas solo es necesario el siguiente comando:

    make pruebaN


Donde N son los posibles valores de las pruebas realizadas, va de 1 a 5.
Hay dos carpetas, una para las pruebas con los tiempos de propagacion  y otras para las pruebas sin estos.
Ademas es importante tomar en cuenta que para ambos casos el clock que se tiene por defecto es con el periodo de 200 ns, si se quieren probar los otros periodos debe cambiarse
en el archivo clock.v
Las pruebas son las mismas de la tarea anterior y son las siguientes:

1. prueba1: Esta es una prueba con valores aleatorios para D, y con modos también aleatorios,con la cual se verifica el funcionamiento de todos los modos, además el enable inicia en 0 yluego se pone en 1, no cambia
    
2. prueba2: Esta es una prueba para ver como se comporta el diseño en las transiciones entremodos, manteniendo el enable como en la prueba anterior, pero ahora los valores de mode secambian cada 4 ciclos.
    
3. prueba3: Esta es una prueba donde se verificara si al variar el valor de enable y ponerlo en0, el contador se coloca en estado de alta impedancia, por lo que cada 3 ciclos del reloj se iraalternando este valor de enable, viendo que pasa con todos los modos.
    
4. prueba4: El valor de reset se va cambiando y el enable se mantiene, esto para confirmar quedeje de contar mientras el reset es 1
    
5. prueba5: Esta es una prueba específica, donde se inicializa el valor de modo en 11, para verificarcomo se comporta el contador iniciando en 11 y luego variando a otro modo y así repetidasveces.
