task checker;

input integer iteration;

repeat (iteration) @ (posedge clk) begin
	if ({sb.sb_Q_checker,sb.sb_rco,sb.sb_load} == {Q, rco,load}) begin
          $fdisplay(log, "PASS");
        end
        else begin
          $fdisplay(log, "Time=%.0f Error dut: Q=%b, rco=%b, load= %b, scoreboard: Q=%b, rco=%b, load= %b", $time, Q, rco, load, sb.sb_Q_checker,sb.sb_rco,sb.sb_load);
        end
end
endtask
