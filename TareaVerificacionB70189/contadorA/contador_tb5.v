`include "scoreboard.v"

module contador_tb(clk, enable, D, mode, Q, rco, reset, load);

output enable;
output [3:0] D;
output [1:0] mode;
output reset;
reg reset;
reg enable;
reg [3:0] D;
reg [1:0] mode;
input clk, rco;
input [3:0] Q;
input load;




`include "driver.v"
`include "checker.v"

parameter ITERATIONS = 50;

integer log;

initial begin


  $dumpfile("verif_contador.vcd");
  $dumpvars(0);

  log = $fopen("tb.log");
  $fdisplay(log, "time=%5d, Simulation Start", $time);
  $fdisplay(log, "time=%5d, Starting Reset", $time);

  drv_init2();

  $fdisplay(log, "time=%5d, Reset Completed", $time);

  $fdisplay(log, "time=%5d, Starting Test", $time);
  fork 
    drv_request5(ITERATIONS);  
    checker(ITERATIONS);
  join
  $fdisplay(log, "time=%5d, Test Completed", $time);
  $fdisplay(log, "time=%5d, Simulation Completed", $time);
  $fclose(log);
  #200 $finish;
end

scoreboard sb(
    .clk (clk),
    .enable (enable),
    .D (D),
    .mode (mode),
    .reset (reset)

);

endmodule
