 task drv_init;
   begin
     @(posedge clk)
      enable = 0;
       reset = 1;
       D=$random;
      mode=$random;
   @(posedge clk)
      enable = 1;
       reset = 0;
     @(posedge clk)
       enable = 1;
   end
 endtask

 task drv_init2;
   begin
     @(posedge clk)
      enable = 0;
       reset = 1;
       D=$random;
      mode=2'b11;
   @(posedge clk)
      enable = 1;
       reset = 0;
     @(posedge clk)
       enable = 1;
   end
 endtask






task drv_request1;
//Prueba donde se realiza el contador completo para cada modo, enable siempre arriba
input integer iteration;
  
  repeat (iteration) begin
    @(posedge clk) begin
    mode <= $random;
    D <= $random;
	
    end
    repeat(17) begin
        @(posedge clk);
        D<=$random;
    end
  end
endtask


 task drv_request2;
// //Prueba donde se realizan transiciones cada 3 ciclos, enable siempre arriba
 input integer iteration;

   repeat (iteration) begin
     @(posedge clk) begin
     mode <= $random;
     D <= $random;
     end
    repeat(3) begin
         @(posedge clk);
         D<=$random;
     end
   end
 endtask

 task drv_request3;
 //Prueba donde se realizan transiciones cada 3 ciclos, enable va a ir cambiando, reset tambien
 input integer iteration;
  
   repeat (iteration) begin
     @(posedge clk) begin
     mode <= $random;
     D <= $random;
     end
     repeat(3) begin
         @(posedge clk);
         D<=$random;
     end
         enable<=!enable;
         repeat(3) begin
         @(posedge clk);
         D<=$random;
     end
         enable<=!enable;
         reset<=!reset;
         reset<=!reset;
         repeat(3) begin
         @(posedge clk);
        D<=$random;
     end
   end
 endtask
 task drv_request4;
 //Prueba donde se realizan transiciones cada 3 ciclos, enable se mantiene y se cambia el reset
 input integer iteration;
  
   repeat (iteration) begin
    @(posedge clk) begin
     mode <= $random;
     D <= $random;
     end
     repeat(3) begin
         @(posedge clk);
         D<=$random;
     end
         reset<=!reset;
         repeat(3) begin
         @(posedge clk);
         D<=$random;
     end
         reset<=!reset;
         repeat(3) begin
        @(posedge clk);
         D<=$random;
     end
   end


 endtask



 task drv_request5;
 //Prueba donde se realizan transiciones, pero se comienza con el modo 11
 input integer iteration;
  
   repeat (iteration) begin
    @(posedge clk) begin
     mode <= 2'b11;
     D <= $random;
     end
     repeat(10) begin
         @(posedge clk);
         D<=$random;
     end
	 mode <= 2'b01;
         repeat(3) begin
         @(posedge clk);
         D<=$random;
     end
	 mode <= 2'b11;
 	 mode <= 2'b10;
         repeat(3) begin
        @(posedge clk);
         D<=$random;
     end
   end


 endtask


